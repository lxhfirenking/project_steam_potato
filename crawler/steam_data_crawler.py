import requests
import json
import time
import logging
import logging.handlers
import os
import threading
from enum import Enum
import sys

class QueryResult(Enum):
    IGNORED_NOT_GAME = 0
    IGNORED_WRONG_ID = 1
    SUCCESS = 2
    FAILED = 3

class Counter():
    def __init__(self):
        self.lock = threading.Lock()
        self.val = 0

    def increment(self):
        with self.lock:
            self.val += 1

    def increment_by(self, value):
        with self.lock:
            self.val += value

    def get_val(self):
        return self.val

class SteamDataCrawler:
    logdir = 'logs'
    game_info_outputdir = 'output/game_info'
    game_review_outputdir = 'output/game_review'
    steamspy_appdetail_outputdir = 'output/steamspy_app_detail_outputdir'
    log_max_byte = 10000000
    log_backup_count = 1000
    query_interval = 2.0
    steamspy_query_interval = 0.3

    def __init__(self, name):
        self.init_output_dir()
        self.init_logging(name)

    def init_output_dir(self):
        if not os.path.exists(self.game_info_outputdir):
            os.mkdir(self.game_info_outputdir)
        if not os.path.exists(self.game_review_outputdir):
            os.mkdir(self.game_review_outputdir)
        if not os.path.exists(self.steamspy_appdetail_outputdir):
            os.mkdir(self.steamspy_appdetail_outputdir)

    def init_logging(self, name):
        if not os.path.exists(self.logdir):
            os.mkdir(self.logdir)
        # create logger with 'spam_application'
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs info messages
        fh_info = logging.handlers.RotatingFileHandler("{0}/{1}_info.log".format(self.logdir, name), maxBytes=self.log_max_byte, backupCount=self.log_backup_count)
        fh_info.setLevel(logging.INFO)

        # create file handler which logs debug messages
        fh_debug = logging.handlers.RotatingFileHandler("{0}/{1}_debug.log".format(self.logdir, name), maxBytes=self.log_max_byte, backupCount=self.log_backup_count)
        fh_debug.setLevel(logging.DEBUG)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.ERROR)
        # create formatter and add it to the handlerss
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh_info.setFormatter(formatter)
        fh_debug.setFormatter(formatter)
        ch.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh_info)
        self.logger.addHandler(fh_debug)
        self.logger.addHandler(ch)



    def get_review(self, game_id):
        url_base="http://store.steampowered.com/appreviews/{}?json=1&start_offset=".format(game_id)
        stop_crawling = False
        start_offset = 0
        l = []
        pre_iter_rec_id = []
        while (start_offset < 3000):
            try:
                time.sleep(self.query_interval)
                url = "{}{}".format(url_base, start_offset)
                start_offset = start_offset + 20

                res = requests.get(url)
                res = res.json()

                if res is None:
                    self.logger.error('null is returned when retrieving reviews for appid={} start_offset={}. The crawler might be throttled'.format(game_id, start_offset))
                    return QueryResult.FAILED, []

                success = res["success"]
                if success and ("reviews" in res):
                    rec_ids = list(map(lambda entry : entry["recommendationid"], res["reviews"]))
                    if (len(rec_ids) == 0):
                        break
                    if (rec_ids[0] in pre_iter_rec_id):
                        break
                    pre_iter_rec_id = rec_ids
                    l.extend(res["reviews"])
                else:
                    self.logger.error('malformed content is returned when retrieving reviews for appid={} start_offset={}. content={}'.format(game_id, start_offset, str(res)[:100]))
                    return QueryResult.FAILED, []

            except Exception as e:
                self.logger.error('Exception occurs when retrieving reviews for appid={} start_offset={} exception={}'.format(game_id, start_offset, e))

        self.logger.debug('successfully obtained {} reviews for appid={}'.format(len(l), game_id))
        return QueryResult.SUCCESS, l

    def get_game_reviews(self, game_ids, start_index, end_index):
        get_game_review_success_counter = Counter()
        get_game_review_failure_counter = Counter()
        total_review_counter = Counter()


        game_review_dict = {}  # <game_id, [reviews]>
        failed_id_list = []  # list of IDs that we failed to get info for
        loop_review_count = 0
        prev_save_index  = start_index

        end_index = min(len(game_ids), end_index)
        for index in range(start_index, end_index):
            id = game_ids[index]
            result, data = self.get_review(id)

            if result == QueryResult.SUCCESS:
                get_game_review_success_counter.increment()
                game_review_dict[id] = data
                loop_review_count = loop_review_count + len(data)
                total_review_counter.increment_by(len(data))
            elif result == QueryResult.FAILED:
                get_game_review_failure_counter.increment()
                failed_id_list.append(id)

            # log telemetries:
            if index % 50 == 0:
                self.logger.info("Proccessed {} games starting from index {}. Success:{} Failure:{}. Total reviws crawled:{}"
                                 .format(index+1 - start_index, start_index, get_game_review_success_counter.get_val(),
                                    get_game_review_failure_counter.get_val(), total_review_counter.get_val()))

            # write to disk every so often
            if (index % 500 == 0 or index == end_index - 1 or loop_review_count > 10000):
                try:
                    #hack
                    self.save_additional_game_review(prev_save_index, index + 1, game_review_dict, failed_id_list)
                    #self.save_game_review(prev_save_index, index + 1, game_review_dict, failed_id_list)
                    game_review_dict = {}
                    failed_id_list = []
                    loop_review_count = 0
                except Exception as e:
                    self.logger.error('Exception while saving reviews to disk for ids from index {} to index {}. Exception:{}'.format(prev_save_index, index + 1, e))
                prev_save_index = index + 1

    def save_game_review(self, start_index, end_index, game_review_dict, failed_id_list):
        # note that the key in the JSON for this dict is of type string, not int
        with open('{}/game_review_dict_{}_to_{}.json'.format(self.game_review_outputdir, start_index, end_index), 'w') as f:
            json.dump(game_review_dict, f)

        with open('{}/failed_id_list_{}_to_{}.json'.format(self.game_review_outputdir, start_index, end_index), 'w') as f:
            json.dump(failed_id_list, f)

    def save_additional_game_review(self, start_index, end_index, game_review_dict, failed_id_list):
        # note that the key in the JSON for this dict is of type string, not int
        with open('{}/additional_game_review_dict_{}_to_{}.json'.format(self.game_review_outputdir, start_index, end_index), 'w') as f:
            json.dump(game_review_dict, f)

        with open('{}/additional_failed_id_list_{}_to_{}.json'.format(self.game_review_outputdir, start_index, end_index), 'w') as f:
            json.dump(failed_id_list, f)

    def get_steamspy_app_detail(self, app_id):
        url = 'http://steamspy.com/api.php?request=appdetails&appid={}'.format(app_id)
        try:
            #headers = {'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            #        'Accept-Encoding':'gzip, deflate',
            #        'Accept-Language':'en-US,en;q=0.9',
            #        'Cache-Control':'max-age=0',
            #        'Connection':'keep-alive',
            #        'Cookie':'__cfduid=d89325c9e090fd640a911a503df6505b11511042736',
            #        'Host':'steamspy.com',
            #        'Upgrade-Insecure-Requests':'1',
            #        'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36}'}
            #res = requests.get(url, headers=headers)
            res = requests.get(url)
            res = res.json()
            if res is None:
                self.logger.error('null is returned when querying appid={} The crawler might be throttled'.format(app_id))
                return QueryResult.FAILED, {}

            if res['name'] is None:
                self.logger.error('invalid content is returned when querying appid={} steam spy might have the the right info'.format(app_id))
                return QueryResult.IGNORED_WRONG_ID, {}
            else:
                return QueryResult.SUCCESS, res
        except Exception as e:
            self.logger.error('Exception occurs when querying appid={} exception={}'.format(app_id, e))
            return QueryResult.FAILED, {}

    def get_steamspy_app_detail_all(self, game_ids, start_index, end_index):
        get_steamspy_app_detail_success_counter = Counter()
        get_steamspy_app_detail_failure_counter = Counter()
        get_steamspy_app_detail_ignored_counter = Counter()

        steamspy_app_detail_dict = {}  # <game_id, [reviews]>
        failed_id_list = []  # list of IDs that we failed to get info for
        prev_save_index  = start_index

        end_index = min(len(game_ids), end_index)
        for index in range(start_index, end_index):
            id = game_ids[index]
            result, data = self.get_steamspy_app_detail(id)

            if result == QueryResult.SUCCESS:
                get_steamspy_app_detail_success_counter.increment()
                steamspy_app_detail_dict[id] = data
            elif result == QueryResult.FAILED:
                get_steamspy_app_detail_failure_counter.increment()
                failed_id_list.append(id)
            elif result == QueryResult.IGNORED_WRONG_ID:
                get_steamspy_app_detail_ignored_counter.increment()

            self.logger.debug('Tried to obtain app detail from steam spy. appid={} result={}'.format(id, result))

            # log telemetries:
            if index % 50 == 0:
                self.logger.info("Proccessed {} gamespy game app detail starting from index {}. Success:{} Failure:{} Ignored:{}."
                                 .format(index+1 - start_index, start_index, get_steamspy_app_detail_success_counter.get_val(),
                                    get_steamspy_app_detail_failure_counter.get_val(), get_steamspy_app_detail_ignored_counter.get_val()))

            # write to disk every so often
            if (index % 500 == 0 or index == end_index - 1):
                try:
                    self.save_steamspy_app_detail(prev_save_index, index + 1, steamspy_app_detail_dict, failed_id_list)
                    steamspy_app_detail_dict = {}
                    failed_id_list = []
                except Exception as e:
                    self.logger.error('Exception while saving steamspy app details to disk for ids from index {} to index {} Exception={}'.format(prev_save_index, index, e))
                prev_save_index = index + 1
            # throttling qps = 2
            time.sleep(self.steamspy_query_interval)

    def save_steamspy_app_detail(self, start_index, end_index, steamspy_app_detail_dict, failed_id_list):
        # note that the key in the JSON for this dict is of type string, not int
        with open('{}/steamspy_app_detail_dict_{}_to_{}.json'.format(self.steamspy_appdetail_outputdir, start_index, end_index), 'w') as f:
            json.dump(steamspy_app_detail_dict, f)

        with open('{}/failed_id_list_{}_to_{}.json'.format(self.steamspy_appdetail_outputdir, start_index, end_index), 'w') as f:
            json.dump(failed_id_list, f)

    def get_app_info(self, app_id):
        app_id = str(app_id)

        url = 'http://store.steampowered.com/api/appdetails?appids={}'.format(app_id)
        self.logger.debug('url: ' + url)

        try:
            res = requests.get(url)
            res = res.json()

            if res is None:
                self.logger.error('null is returned when querying appid={} The crawler might be throttled'.format(app_id))
                return QueryResult.FAILED, {}

            success = res[app_id]['success']

            if success:
                if res[app_id]['data']['type'] == 'game':
                    return QueryResult.SUCCESS, res[app_id]['data']
                else:
                    return QueryResult.IGNORED_NOT_GAME, {}
            else:
                return QueryResult.IGNORED_WRONG_ID, {}
        except Exception as e:
            self.logger.error('Exception occurs when querying appid={} exception={}'.format(app_id, e))
            return QueryResult.FAILED, {}

    def save_game_info(self, start_index, end_index, game_id_list, game_info_dict, failed_id_list):
        with open('{}/game_id_list_{}_to_{}.json'.format(self.game_info_outputdir, start_index, end_index), 'w') as f:
            json.dump(game_id_list, f)

        # note that the key in the JSON for this dict is of type string, not int
        with open('{}/game_info_dict_{}_to_{}.json'.format(self.game_info_outputdir, start_index, end_index), 'w') as f:
            json.dump(game_info_dict, f)

        with open('{}/failed_id_list_{}_to_{}.json'.format(self.game_info_outputdir, start_index, end_index), 'w') as f:
            json.dump(failed_id_list, f)

    def get_game_ids_and_info(self, start_index, end_index):
        get_app_info_success_counter = Counter()
        get_app_info_failure_counter = Counter()
        get_app_info_ignored_notgame_counter = Counter()
        get_app_info_ignored_wrongid_counter = Counter()

        with open('data/all_steam_app_id.json', 'r') as f:
            app_list = json.load(f,encoding='utf8')
            app_list = app_list['applist']['apps']

        self.logger.info("obtained {} app ids...".format(len(app_list)))
        self.logger.info("will proccess app id from index {} to {}".format(start_index, end_index))

        game_id_list = []
        game_info_dict = {}  # <game_id, data>
        failed_id_list = []  # list of IDs that we failed to get info for
        prev_save_index  = start_index

        end_index = min(len(app_list), end_index)
        for index in range(start_index, end_index):
            app = app_list[index]
            id = app['appid']
            result, data = self.get_app_info(id)

            if result == QueryResult.SUCCESS:
                get_app_info_success_counter.increment()
                game_id_list.append(id)
                game_info_dict[id] = data
            elif result == QueryResult.IGNORED_NOT_GAME:
                # not a game
                get_app_info_ignored_notgame_counter.increment()
            elif result == QueryResult.IGNORED_WRONG_ID:
                get_app_info_ignored_wrongid_counter.increment()
            elif result == QueryResult.FAILED:
                get_app_info_failure_counter.increment()
                failed_id_list.append(id)

            # log telemetries:
            if index % 50 == 0:
                self.logger.info("Proccessed {} games starting from index {}. Success:{} Failure:{} Ignored_Not_Game:{} Ignored_Wrong_Id:{}"
                                 .format(index+1 - start_index, start_index, get_app_info_success_counter.get_val(), get_app_info_failure_counter.get_val(),
                                    get_app_info_ignored_notgame_counter.get_val(), get_app_info_ignored_wrongid_counter.get_val()))

            # write to disk every so often
            if (index % 500 == 0 or index == end_index - 1):
                try:
                    self.save_game_info(prev_save_index, index + 1, game_id_list, game_info_dict, failed_id_list)
                    game_id_list = []
                    game_info_dict = {}
                    failed_id_list = []
                except Exception as e:
                    self.logger.error('Exception while saving to disk for the 500 IDs up to {}'.format(index))
                prev_save_index = index + 1
            time.sleep(self.query_interval)

    def get_all_valid_game_id(self):
        l = []
        for filename in os.listdir(self.game_info_outputdir):
            if (filename.startswith("game_id_list_") and filename.endswith(".json")):
                full_filename = "{}/{}".format(self.game_info_outputdir, filename)
                with open(full_filename, 'r') as f:
                    app_list = json.load(f,encoding='utf8')
                    l.extend(app_list)
        #self.logger.info("obtained {} valid game ids".format(len(l)))
        #must sort s.t. the order is the same across all runs
        l.sort()
        print("obtained {} valid game ids".format(len(l)))
        return l

    def get_missing_review_game_id(self):
        with open("output/games_with_no_reviews.json", 'r') as f:
            l = sorted(json.load(f,encoding='utf8'))
        print("obtained {} missing review game ids".format(len(l)))
        return l


def main():
    start_index = int(sys.argv[1])
    end_index = int(sys.argv[2])
    c = SteamDataCrawler("steam_data_crawler")
    #ids = c.get_all_valid_game_id()
    ##c.get_steamspy_app_detail_all(ids, start_index, end_index)
    # this is a hack to fix some previous errors...
    ids = c.get_missing_review_game_id()
    c.get_game_reviews(ids, start_index, end_index)
    ##c.get_game_ids_and_info(start_index, end_index)

if __name__ == '__main__':
    main()
