import requests
import json
import time



def get_app_info(app_id):
    app_id = str(app_id)

    url = 'http://store.steampowered.com/api/appdetails?appids={}'.format(app_id)
    print('url: ' + url)

    res = requests.get(url)
    res = res.json()

    if res is None:
        print('res is None, logging res:' + str(res))
        return 2, {}

    success = res[app_id]['success']

    if success:
        if res[app_id]['data']['type'] == 'game':
            return 0, res[app_id]['data']
        else:
            return 1, {}
    else:
        return 2, {}


def save_game_info(cur_index, game_id_list, game_info_dict, failed_id_list):
    with open('output/game_id_list_{}.json'.format(cur_index), 'w') as f:
        json.dump(game_id_list, f)

    # note that the key in the JSON for this dict is of type string, not int
    with open('output/game_info_dict_{}.json'.format(cur_index), 'w') as f:
        json.dump(game_info_dict, f)

    with open('output/failed_id_list_{}.json'.format(cur_index), 'w') as f:
        json.dump(failed_id_list, f)


def get_game_ids_and_info():
    with open('data/all_steam_app_id.json', 'r') as f:
        app_list = json.load(f)
        app_list = app_list['applist']['apps']

    app_list = app_list[3200:]  # for debugging or restart

    game_id_list = []
    game_info_dict = {}  # <game_id, data>
    failed_id_list = []  # list of IDs that we failed to get info for

    for index, app in enumerate(app_list):
        print('{} out of {}'.format(index, len(app_list)))

        time.sleep(1.6)

        id = app['appid']
        result, data = get_app_info(id)

        if result == 0:
            # is a game
            game_id_list.append(id)
            game_info_dict[id] = data

        elif result == 1:
            # not a game
            pass
        else:
            failed_id_list.append(id)

        # write to disk every so often
        if index % 1000 == 0:
            try:
                save_game_info(index, game_id_list, game_info_dict, failed_id_list)
                game_id_list = []
                game_info_dict = {}
                failed_id_list = []
            except Exception as e:
                print('Exception while saving to disk for the 1000 IDs up to {}'.format(index))

    save_game_info(index, game_id_list, game_info_dict, failed_id_list)

def get_review(game_id):
    pass

def main():
    get_game_ids_and_info()

if __name__ == '__main__':
    main()