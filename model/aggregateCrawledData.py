import pandas as pd
import os
import json
from collections import defaultdict
from datetime import datetime
import dateutil.parser
import csv
import math
from utils import sort_dict_descending


# From game_info, obtain information such as publishers, and filter out games that are released in the past 3 months
# and games that are not 'single-player'.

game_reviews_path = '../output/data_reviews/all_reviews.json'  # '../output/game_review/'

output_folder_path = '../output/data_frames_all_reviews/'

# constant paths:

game_info_path = '../output/game_info/'

steam_spy_path = '../output/steamspy_app_detail_outputdir/'

genres_encoding_path = '../output/data_frames/genres_encoding.json'

date_release = datetime.strptime('Aug 10, 2017', '%b %d, %Y')


def is_single_player_game(game_info):
    if 'categories' in game_info:
        for category in game_info['categories']:
            if category['description'] == 'Single-player':
                return True
    else:
        print('drop because !is_single_player_game')
        return False


def is_released_a_while_back(game_info):
    if 'release_date' in game_info:
        release = game_info['release_date']
        if release['coming_soon']:
            return False

        try:
            # date format is "Aug 3, 2016" or "7 Jul, 2015"
            # date = datetime.strptime(release['date'], '%b %d, %Y')
            date = dateutil.parser.parse(release['date'])
            if date < date_release:
                return True
            else:
                print(date)
                return False
        except Exception as e:
            print(e)
            print(game_info['name'])
            return False
    else:
        print('drop because no release_date')
        return False


def is_paid_game(game_info):
    if 'is_free' in game_info:
        if not game_info['is_free']:
            return True
    else:
        print('drop because !is_paid_game')
        return False


def get_total_achievements(game_info):
    if 'achievements' in game_info:
        if 'total' in game_info['achievements']:
            return game_info['achievements']['total']
    return 0


def aggregate_genres(game_info, genres_all):
    if 'genres' in game_info:
        for genre_info in game_info['genres']:
            genres_all[genre_info['description']] += 1


def aggregate_publishers(game_info, publishers_all):
    if 'publishers' in game_info:
        for publisher in game_info['publishers']:
            publishers_all[publisher] += 1


def aggregate_developers(game_info, developers_all):
    if 'developers' in game_info:
        for developer in game_info['developers']:
            developers_all[developer] += 1

def aggregate_categories(game_info, categories_all):
    if 'categories' in game_info:
        for category in game_info['categories']:
            if ('description' in category):
                categories_all[category['description']] += 1


def has_all_needed_fields(game_info):
    required_keys = {'name', 'price_overview', 'developers', 'genres'}
    if required_keys.issubset(game_info):
        return True
    return False


def get_publishers(game_info):
    if 'publishers' in game_info:

        pub = game_info['publishers'][0]

        if pub.isspace() or pub == '' or pub == '-' or pub == '(none)' or format_text(pub) == 'n/a':
            return format_array_text(game_info['developers'])
        else:
            return format_array_text(game_info['publishers'])
    else:
        return format_array_text(game_info['developers'])


def strip_leading_trailing_white_space(game_info):
    pass


def format_text(text):
    return text.strip().lower()


def format_array_text(array):
    return list(map(lambda x: format_text(x), array))

def format_categories(array):
    return list(map(lambda x: format_text(x['description']), array))

def parse_game_info():
    games = defaultdict(lambda: defaultdict(str))  # dict of dict, each dict will be one row in the data frame
    genres = defaultdict(int)  # how many games had this genre
    publishers = defaultdict(int)
    developers = defaultdict(int)
    categories = defaultdict(int)

    for file_name in os.listdir(game_info_path):
        # print('parse_game_info, file_name: ' + file_name)
        if 'game_info_dict' in file_name:
            with open(game_info_path + file_name, 'r') as f:
                game_infos = json.load(f)

            for game_id, game_info in game_infos.items():
                if has_all_needed_fields(game_info) \
                        and is_single_player_game(game_info) \
                        and is_paid_game(game_info) \
                        and is_released_a_while_back(game_info):

                    game_id = int(game_id)

                    games[game_id] = {
                        'game_id': game_id,
                        'name': format_text(game_info['name']),
                        'initial_price': game_info['price_overview']['initial'],
                        'categories': '~'.join(format_categories(game_info['categories'])),
                        'discount_percent': game_info['price_overview']['discount_percent'],
                        'total_achievements': get_total_achievements(game_info),
                        'publishers': '~'.join(get_publishers(game_info)),  # these two features are lists of strings
                        'developers': '~'.join(format_array_text(game_info['developers'])),
                        'genres': '~'.join([x['description'] for x in game_info['genres']]),
                        'recommendations': game_info['recommendations']['total'] if 'recommendations' in game_info else 0
                    }

                    aggregate_genres(game_info, genres)
                    aggregate_publishers(game_info, publishers)
                    aggregate_developers(game_info, developers)
                    aggregate_categories(game_info, categories)

    return games, genres, publishers, developers, categories


def aggregate_tags(spy_info, tags_all):
    try:
        if (len(spy_info['tags']) > 0):
            for tag, _ in spy_info['tags'].items():
                tags_all[tag] += 1
            return list(spy_info['tags'].keys())
        else:
            return []
    except Exception as e:
        print(e)


def parse_steam_spy_info(games):
    tags_all = defaultdict(int)  # how many games had this tag

    for file_name in os.listdir(steam_spy_path):
        print('parse_steam_spy_info, file_name: ' + file_name)
        if 'steamspy_app_detail' in file_name:
            with open(steam_spy_path + file_name, 'r') as f:
                game_infos = json.load(f)

            for game_id, spy_info in game_infos.items():
                game_id = int(game_id)
                if game_id in games:
                    game_info = games[game_id]

                    game_info['average_forever'] = spy_info['average_forever']
                    game_info['median_forever'] = spy_info['median_forever']

                    game_info['owners'] = spy_info['players_forever']  # name changed
                    game_info['tags'] = '~'.join(aggregate_tags(spy_info, tags_all))

    return games, tags_all


# def parse_game_reviews(games):
#     with open(game_reviews_path, 'r') as f:
#         all_reviews = json.load(f)
#
#     print('Number of games that had reviews: ' + str(len(all_reviews)))
#
#     games_with_no_reviews = []
#
#     for game_id, game_info in games.items():
#         if str(game_id) not in all_reviews:
#             games_with_no_reviews.append(game_id)
#
#     with open('../output/games_with_no_reviews.json', 'w') as f:
#         json.dump(games_with_no_reviews, f)


def parse_game_reviews(games):
    with open(game_reviews_path, 'r') as f:
        all_reviews = json.load(f)

    print('Number of games that had reviews: ' + str(len(all_reviews)))

    games_with_no_reviews = []
    for game_id, game_info in games.items():
        if str(game_id) in all_reviews:
            reviews_for_game = all_reviews[str(game_id)]

            recommends = 0.0  # weighted by weighted_vote_score
            not_recommends = 0.0

            for review in reviews_for_game:
                if 'weighted_vote_score' in review and 'voted_up' in review:
                    score = float(review['weighted_vote_score'])
                    if (review['voted_up']):
                        recommends += score
                    else:
                        not_recommends += score

            game_info['votes_up'] = recommends
            game_info['votes_down'] = not_recommends
        else:
            games_with_no_reviews.append(game_id)

    with open('../output/games_with_no_reviews.json', 'w') as f:
        json.dump(games_with_no_reviews, f)
    return games


def is_of_genre(row, genre):
    if genre in row['genres']:
        return True
    else:
        return False


# certain games with the same name and other information show up with different game IDs
# drop all duplicate rows and only keep the row with the highest number in owners
# https://stackoverflow.com/questions/12497402/python-pandas-remove-duplicates-by-columns-a-keeping-the-row-with-the-highest
def drop_duplicate_rows(df):
    df.sort_values('owners', ascending=False, inplace=True)

    print(df.head())

    df.drop_duplicates('name', inplace=True)

    df.sort_index(inplace=True)

    print(df.head())


def aggregate_crawled_data(save_files=False):
    games, genres, publishers, developers, categories = parse_game_info()
    games, tags = parse_steam_spy_info(games)
    games = parse_game_reviews(games)

    df = pd.DataFrame(list(games.values()), index=list(games.keys()))
    print('Before dropping duplicated rows, shape: ' + str(df.shape))

    drop_duplicate_rows(df)
    print('After dropping duplicated rows, shape: ' + str(df.shape))

    # only keep games with more than 100 owners
    df = df[df['owners'] > 1000]
    print('After dropping games with fewer than 1000 owners, shape: ' + str(df.shape))

    # # drop games whose sqrt(players_forever_variance) / players_forever > 15%
    # df = df[math.sqrt(df['players_forever_variance']) / df['players_forever'] > 0.15]

    df.to_csv(output_folder_path + 'df_original_deduped.csv')

    if save_files:

        with open(output_folder_path + 'genres.json', 'w') as f:
            json.dump(genres, f)

        with open(output_folder_path + 'tags.json', 'w') as f:
            json.dump(tags, f)

        with open(output_folder_path + 'categories.json', 'w') as f:
            json.dump(categories, f)

        with open(output_folder_path + 'genres.csv', 'w') as f:
            writer = csv.writer(f)
            writer.writerow(['genre', 'number_of_games'])
            for genre, num in genres.items():
                writer.writerow([genre, num])

        with open(output_folder_path + 'tags.csv', 'w') as f:
            writer = csv.writer(f)
            writer.writerow(['tag', 'number_of_games'])
            for tag, num in tags.items():
                writer.writerow([tag, num])

        with open(output_folder_path + 'publishers.csv', 'w') as f:
            writer = csv.writer(f)
            writer.writerow(['publisher', 'number_of_games'])
            for publisher, num in publishers.items():
                writer.writerow([publisher, num])

        with open(output_folder_path + 'developers.csv', 'w') as f:
            writer = csv.writer(f)
            writer.writerow(['developer', 'number_of_games'])
            for developer, num in developers.items():
                writer.writerow([developer, num])

        # genres = sort_dict_descending(genres)
        # genres_encoding = {}
        # for index, tup in enumerate(genres):
        #     genres_encoding[tup[0]] = index
        #
        # with open(genres_encoding_path, 'w') as f:
        #     json.dump(genres_encoding, f)
        #
        # tags = sort_dict_descending(tags)
        # tags_encoding = {}
        # for index, tup in enumerate(tags):
        #     tags_encoding[tup[0]] = index
        #
        # with open('../output/data_frames/tags_encoding.json', 'w') as f:
        #     json.dump(tags_encoding, f)


def main():
    aggregate_crawled_data(save_files=False)


if __name__ == "__main__":
    main()
