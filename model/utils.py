import operator

def sort_dict_descending(dictionary):
    """ Sorts a dict by value in descending order to return a list of tuples

    Args:
        dictionary: The dict to sort.

    Returns:
        The list of tuples sorted by value.
    """
    return sorted(dictionary.items(), key=operator.itemgetter(1), reverse=True)

